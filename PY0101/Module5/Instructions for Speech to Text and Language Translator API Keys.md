<img src= "images/idsn_logo.PNG" width = 300>

Objective for Exercise:
* How to create a IBM Cloud account.
* How to create a Speech to Text Instance.
* How to create a Language Translator Instance.

### Instructions for Speech to Text and Language Translator API Keys

This is the instruction to create an instance of IBM Speech to Text, and IBM Language Translator. IBM Speech to Text is a service that converts speech in audio format into text. Watson Speech to Text is a service that uses deep learning algorithms to convert speech to text. IBM Language Translator converts one language to another.

NOTE: In order to complete this the lab in this section you will be creating an IBM Cloud account and create an instance of IBM Speech to Text and IBM Language Translator and obtain their respective API Keys. A credit card is NOT required to sign up for IBM Cloud Lite account and there is no charge associated in creating a Lite plan instance of the Watson VR service.

#### Step 1: Create an IBM Cloud Account

Click on the link below to create an IBM cloud account:
[Sign Up for IBM Cloud](https://cloud.ibm.com/registration).
On the page to which you get redirected by clicking on the link above, enter your **email address, first name, last name, country or region**, and set your password.

NOTE: To get enhanced benefits, please sign up with you company email address rather than a free email ID like Gmail, Hotmail, etc.

If you would like IBM to contact you for any changes to services or new offerings, then check the box to accept the box to get notified by email. Then click on the Create Account button to create your IBM Cloud account. **If you already have an IBM Cloud account you can just log in using the link above the Email field (top right in the screenshot below).

<img src="images/5.1.PNG"  width = 600>

#### Step 2: Confirm Your Email Address

An email is sent to your email address to confirm your account.

<img src="images/5.2.PNG"  width = 600>

Go to your email account, and click on the **"Confirm Account"** link in the email that was sent to you.

<img src="images/5.3.PNG"  width = 600>

#### Step 3: Login to Your Account

<img src="images/5.4.PNG"  width = 600>

When you click Log in, you will be redirected to a page to log into your IBM Cloud account.

<img src="images/5.5.PNG"  width = 600>

#### Step 4: Create a Speech to Text Instance

To create a speech to text instance click [Speech to Text](https://cloud.ibm.com/catalog/services/speech-to-text)

On the next page, you will get to name your service instance and choose your region. Click on the arrow to reveal the drop-down menu of regions. Make sure to select the region that is closest to you. Since I am located in Canada, then I am choosing Dallas as my region since it is the closest region to me.

<img src="images/5.6.PNG"  width = 600>

Then scroll down and make sure that the **lite** plan is selected, and click the **Create** button.

<img src="images/5.7.PNG"  width = 600>

#### Step 5: Save your Speech to Text API Key
Go to **Manage**, then save your Speech to Text **API Key**, and **URL** you will need it for the labs in this section.

<img src="images/5.8.PNG"  width = 600>

#### Step 6: Create a Language Translator Instance
To create a translation instance click [Language Translator](https://cloud.ibm.com/catalog/services/language-translator)

On the next page, you will get to name your service instance and choose your region. Click on the arrow to reveal the drop-down menu of regions. Make sure to select the region that is closest to you. Since I am located in Canada, then I am choosing Dallas as my region since it is the closest region to me.

<img src="images/5.9.PNG"  width = 600>


Then scroll down and make sure that the **lite** plan is selected, and click the **Create** button.

<img src="images/5.10.PNG"  width = 600>

#### Step 7: Save your Language Translator API Key

Go to **Manage**, then save your Language Translator **API Key**, and **URL** you will need it for the labs in this section.

<img src="images/5.11.PNG"  width = 600>


## Author(s)
[Joseph Santarcangelo](https://www.linkedin.com/in/joseph-s-50398b136/)


## Changelog
| Date | Version | Changed by | Change Description |
|------|--------|--------|---------|
| 2020-09-01 | 2.0 | Srishti | Migrated Lab to Markdown and added to course repo in GitLab |
|   |   |   |   |
|   |   |   |   |


## <h3 align="center"> © IBM Corporation 2020. All rights reserved. <h3/>













