Final Quiz  
 
Question 1. How Cloud Billing Reports can be helpful?  
1. To generate reports based on the amount spent by region. 
2. To know which Google Cloud project cost the most last month. 
3. **_Both 1 and 2 _**
4. None of these 
 
Question 2. The amount of data processed by each query you run determines the on-demand cost. 
1. True 
2. _False _****
 
Question 3. Which prediction-related fields represent an array of pairs that shows the probability of each shot being successful and not from the model? 
1. predicted_isGoal_probs 
2. predicted_isGoal 
3. predicted_isGoal_pairs 
4.None of these 
 
Question 4. Which of the two metrics are useful for tracking things like readability or measuring how many of your most key terms were accurately transcribed? 
1. Jaccard Index and F1 score 
2. F1 score and Precision 
3. Accuracy and Jaccard Index 
4. Precision and Accuracy 
 
Question 5. What are the two pricing models for query costs in BigQuery? 
1. On-demand 
2. Flat-rate 
3. Both 1 and 2 
4. None of these 
