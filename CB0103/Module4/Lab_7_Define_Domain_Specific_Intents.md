<img src= "images/IDSNlogo.PNG" width = 300>

#### Lab 7 - Define Domain-Specific Intents

# Exercise 1: Respond to hours of operation requests

Chit chat interactions are necessary to make our chatbot more pleasant and human-like.
However, what makes the chatbot actually useful is its ability to answer domain-specific
questions. That is, business-related questions.

We defined intents for people inquiring about hours of operation and addresses of our fictional
florist chain and even created an entity to be able to provide location-specific answers.
However, much like the chit chat intents, intents alone don't offer responses to customers.
We'll need to create nodes to handle these two business-specific intents.

## Create the parent node

We'll start by creating a node for hours of operation. Follow these steps:

1. From the Dialog section, **click on the** **_Add node_** **button**. This will create an empty node just
below the first node in the dialog.

If the node was created elsewhere in the dialog, you know by now how to move it just below
the _Welcome_ node (hint: find the option in the three dotted/more options menu for that node).

2. **Set the node name to Hours of Operation** and **use #hours_info for the node condition**.
This will ensure that the node will be executed when the user is inquiring about shop hours.
3. **In the response, enter:**

Our hours of operations are listed on <a
href="https://example.org/hours/">our Hours page</a>.

Notice how HTML code is allowed in responses, enabling us to provide more interactive and
useful textual answers.
```
Later, when you test this link, you’ll be able to click on it and land on a sample page. If you were
to get an error instead, consider replacing the double quotes with single quotes in the HTML
above.
```
Next, close the node and head over to the _Try it out_ panel to test that it works by asking:

When is your Vancouver store open?

<img src="images/7.1.PNG" width="500">

It should hit the _Hours of Operation_ node and give you its response as shown in the image
above.

This works and it provides a somewhat useful answer to the user (assuming we are pointing
them to a page with the right information listed). However, it feels... not very smart.

After all, the user asked us about a specific location. We even detected it with
the @location entity and then proceeded to ignore it, opting instead for a generic answer. We
can do better than that! (Close the _Try it out_ panel to gain some breathing room as we work on
the dialog.)

In order to handle this case properly, we'll have to consider two possible scenarios. One in
which one of our locations is specified and one in which the user asks about hours of operation
in general without indicating a city or indicating a city in which we don’t have a store.

In the early days of Watson Assistant, before more powerful features were introduced, this
scenario would be handled with child nodes. So, we’ll use child nodes here and I’ll show you the
more advanced alternatives later on in the course.

We'll use our current node to capture the hours of operation request, and then jump to the
child nodes to decide how to handle the request on the basis of the specific location
information that was or wasn't provided.


By the way, as the complexity of your dialog grows, you might have a hard time finding which
node executed the response you're seeing during troubleshooting. To help you out, you can
click on the map pin icon next to the response, and the current node will be highlighted for you.

<img src="images/7.2.PNG" width="300">

## Create the Our Locations child node

1. **Delete the response section from our** **_Hours of Operation_** **node** by clicking on the trash bin
icon in the _Assistant responds_ section. We do that because we don't want this parent node to
provide the answer. We'll let the child nodes decide what's the right response.

2. Close the, now response-less, node. Using the more options menu for the _Hours of Operation_
menu, **select the** **_Add child node_** **option** , as shown in the screenshot below.

<img src="images/7.3.PNG" width="500">

This creates the first child node. We'll use it for the case of the user providing us a city for which
we have a flower shop. So, go ahead and **name it Our Locations**.

3. **Set the condition to @location** , as we want to execute this node only if the user is inquiring
about hours of operation for one of our locations.

As a reminder, a child node is only executed if the parent node's condition was true, since we
typically skip or jump to child nodes from the parent node. Or in the much less common case of
another node jumping to it. In our dialog, if we are executing this child node, we can be certain
about two conditions regarding the user input:

a) The intent will be #hours_info (because the parent node must have been executed to
jump to this child node);


b) The input will contain the @location entity (since that’s the condition for this node to
be executed).

Knowing this allows us to provide very specific responses.

(Okay, technically we haven’t made the parent, _Hours of Operation_ , jump to its first child, _Our
Locations_ yet. Fear not, we’ll do so as soon as we are finished setting up this child node.)

4. We need a way to offer a different response for each city, so we need to enable _Multiple
conditioned responses_. To do so, **click on the** **_Customize_** **link** within the _Our Locations_ child
node. Scroll all the way to the bottom and **switch on** **_Multiple conditioned responses_** **and
click** **_Apply_**. You'll notice that now we have the ability to attach a condition to each response, as
shown below.

<img src="images/7.4.PNG" width="300">

5. Go ahead and **create a series of responses, one for each city**. In the _IF ASSISTANT
RECOGNIZES_ column you'll want to enter the specific city (e.g., @location:Toronto) and in
the _RESPOND WITH_ the hours of our fictional flower shop location (e.g., Our Toronto store
is open Monday to Saturday from 9 am until 6 pm, except statutory holidays.)

**Use** **_Add response_** **to add additional entries** for each location we have. Feel free to come up
with fictional hours of operation, as it is, after all, a fictional retail chain. The end result should
be similar to the image below.

<img src="images/7.5.PNG" width="500">

It's worth noting that if the hours of operations where the same for all locations, we could have
saved the trouble of switching to multiple conditioned responses and simply
included @location in our response. (e.g., Our @location store is open Monday to
Saturday from 9 am until 6 pm, except statutory holidays.)

This would automatically output the detected entity value back to the user in the response. So,
when enquiring about Calgary, the user would receive the response Our Calgary store is
open Monday to Saturday from 9 am until 6 pm, except statutory holidays. Of


course, if stores have different hours of operation, we need to opt for the multiple response
approach like we did here.

**Close the** **_Our Locations_** **node** by clicking on X in the top corner.

## Skip the user input and evaluate the child nodes

Now that we have our child node defined, we need to set the jump from its parent node.

In other words, we need to make sure that the parent node (i.e., _Hours of Operation_ ) hands off
control to the child nodes. We’ll jump to the first node, and if the condition for that node is
false, we’ll continue evaluating other child nodes (that we’ll define in a moment) until one child
node’s condition is met and that child node is finally executed.

**Select the** **_Hours of Operation_** **node** , and you'll notice that the _Then Assistant should_ section is
set to _Wait for reply_. This is not what we want. The user has already provided us with the
question, and we haven't responded yet, since this node has no response.

**Change this section of** **_Hours of Operation_** **to** **_Skip user input_****.** This will hand off the execution
to the child nodes.

<img src="images/7.6.PNG" width="500">

Finally, close the node.

## Create the No Location child node

We now have a way to handle users asking about hours of operation for a specific location of
ours, however, we also need a child node to handle the case in which the user didn't specify a
location (or for a location that we don’t recognize).

1. **Using the more options menu for the** **_Our Locations_** **node, select** **_Add node below_** **to create
a new node.** This will create a node below _Our Locations_ , and _Hours of Operation_ will end up
with two child nodes in total.

<img src="images/7.7.PNG" width="500">

2. **Call this node No Location.** Set its condition to true.

Here is why. When the user asks, “What are your hours of operation?” the #hours_info intent
is detected, so when the dialog is evaluated, we enter the parent node _Hours of Operation_.

The _Our Location_ child node is then first evaluated. We fail its condition because the user didn't
specify any location, so the next child node is considered for execution.

Since the condition is set to true it will automatically be executed. This is exactly what we want
to happen since at this point, we know the user wants to know the hours of operation, but no


location of ours was provided. (If we left the condition empty, we'd get an error because
no child node was able to match the user request.)

Note that the order of your nodes can matter. For example, placing the _No Location_ node with
its _true_ condition above _Our Locations_ would overshadow _Our Locations_ and it would never be
executed. Instead, _No Location_ would be executed each time, which is not what we want if the
user specified one our locations.

So, the order of our nodes didn’t matter in the case of the chit chat nodes, but it matters here.
The general rule is to always put specific cases above more generic cases, to avoid
overshadowing the specific nodes with the more generic ones.

3. We need a generic answer for when no location is specified, so **go ahead and reuse the
message we had originally.**

Our hours of operations are listed on <a
href="https://example.org/hours/">our Hours page</a>.

<img src="images/7.8.PNG" width="500">


4. Click on the _Try it_ button, click _Clear_ , then try the following inputs (one at a time):

What are you hours of operation in Toronto?

What are your hours in Calgary?

What are your hours in Seattle?

What are your hours of operation?

<img src="images/7.9.PNG" width="300">

You should see a proper response for each of these inputs. Not bad!


# Exercise 2: Respond to address requests

Our little chatbot is getting more useful by the minute. We now need to handle location
address requests. And guess what? It's no different in terms of how this works. We'll have a
parent node and two children to distinguish both scenarios.

By the way, at any time, you can click on the arrow on the left of the _Hours of Operation_ node
to collapse or expand its children.

<img src="images/7.10.PNG" width="500">

**Collapse its child nodes now** to gain some breathing room.

In the new parent and child nodes to handle address requests we’ll need to change one
condition (#location_info instead of #hours_info) and change the responses from hours of
operation to actual addresses. Overall though, the structure is identical.

So, you could redo the process in Exercise 1 above, step by step, with those two minor
adjustments, or we can be more efficient and simply duplicate _Hours of Operation_ and change
its copy to our needs.

We'll opt for this more efficient route:


1. **Click on the more options menu for the** **_Hours of Operation_** **node** and **select** **_Duplicate_** to
make a copy of _Hours of Operation_ and its children.

<img src="images/7.11.PNG" width="500">

2. **Select the** **_Hours of Operation - copy1_** **node** that was generated. **Change its name
to Location Information and its condition to #location_info.** Its name will allow us to
distinguish it from its remarkably similar sibling _Hours of Operation_ , and the condition will
ensure that Watson will only execute the node when the user asks for an address not hours of
operation.

3. Next, we'll need to **change the responses in two child nodes within the** **_Location
Information_** **tree**.

Feel free to get creative but here is the type of response you should assign to each city in
the _Location Information > Our Locations_ child node:

Our Toronto store is located at 123 Warden Avenue.

Add fictitious address for all the cities in that node.

<img src="images/7.12.PNG" width="500">

Likewise, in the more generic _Location Information > No Location_ child node replace the
response to say:

Our store locations are listed on our site on the <a
href="https://example.org/stores">stores page</a>.

4. Open the _Try it out_ panel, press _Clear_ to start a new conversation, and **test out a full
conversation** a user might have with our chatbot. Enter in succession the following input.


hello

where are you stores located?

what are your hours of operations in Montreal?

thank you

bye

If you followed the instructions so far, you should have a pretty neat conversation. We can, of
course, flesh out our chatbot much more, but if you got to this point, you have mastered the
fundamentals of what you need to know to create something useful that cuts down of many
common inquiries from your customers.

We'll soon see how to deploy the chatbot, and then tackle more advanced topics in the process
of improving the chatbot’s usefulness and apparent degree of intelligence.

## Are child nodes really necessary here?

Technically speaking we don't need child nodes to handle the two scenarios we implemented
above. We could simply add multiple conditional responses to the parent nodes and add
responses for each of the cities and the catch-all true case, all from within the same node.

However, I wanted to show you how to work with child nodes, the importance of their
ordering, and their flexibility. If the logic was more complex than just a generic response, having
a dedicated child node to handle it would likely be a good idea, anyway. In some complex
chatbots, you might even have child nodes that have their own child nodes!

Later in the course, we'll get rid of child nodes in favor of something called _Slots_. For now,
please keep the two child nodes below both _Hours of Operation_ and _Location Information_ , as
you defined them in this lab.


