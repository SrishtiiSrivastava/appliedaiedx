<img src ="images/IDSNlogo.PNG" width="300" >

#### Lab 8: Add a preview and retrieve your credentials

The small chatbot we built so far works well enough from the _Try it out_ panel. That's great and
all but our customers won't have access to it unless we deploy it somewhere. Let's see how to
accomplish that.

# Exercise 1: Add a Preview link

Assistants within Watson Assistant have an _Integrations_ section from which we can select
various way to deploy our chatbot. Before we look at how to deploy our chatbot on WordPress,
in particular, it would be good to take advantage of the _Preview link_ integration.

This _Preview link_ can be shared with friends and colleagues who'd like to try out your chatbot.

To enable the _Preview link_ , first **head over to the** **_Assistants_** **tab**. You’ll notice that 0 next to
Integrations. That’s because we don’t have any integrations enabled quite yet. **Click on the tile
for your chatbot.**

<img src="images/8.1.PNG" width="600">

From within your assistant, **click on** **_Add integration_** **in the** **_Integrations_** **section**.


A new page will appear showing you various options, including Facebook Messenger, Slack,
Intercom, etc. **Click on** **_Preview link_** **under** **_Stand-alone integrations_** **.**

<img src="images/8.2.PNG" width="600">

Rename it, if you wish, and then **click on the** **_Create_** **button**.

<img src="images/8.3.PNG" width="600">

A link will be generated for you. **Click on it.** That’s your _Preview link_ that you can share with
others who are interested in trying out your chatbot.

Do bear in mind that every time someone sends a message to the chatbot, one API call is made,
and it counts towards your free allowance (10,000 API calls per month in the Lite plan).

Test it out to verify that the chatbot we built so far, does indeed work correctly from this user
interface.

<img src="images/8.4.PNG" width="600">

Go back to your _Preview link integration_ (typically in an existing browser tab) and **click on** **_Save
changes_** **.**

<img src="images/8.5.PNG" width="600">

This will add the preview link to your Integrations.

<img src="images/8.6.PNG" width="600">

# Exercise 2: Retrieve your chatbot credentials

The preview link is quite handy but to actually deploy our chatbot in production, we’ll want to
collect our assistant’s credentials and make note of them.

**Click on the more options menu for your** **_Flower Shop Chatbot_** **assistant, then select** **_Settings_** **.**

<img src="images/8.7.PNG" width="600">

From the settings, **click on** **_API Details_** **.**


**Make note of the** **_Assistant URL_** **and the** **_API Key_** **.** You'll need to know them in order to
successfully deploy your chatbot later on.

<img src="images/8.8.PNG" width="500">

**Make note of them now** and then click on the X to close the API credentials page.

# Exercise 3: Generate a WordPress site

You followed along and now have a simple Flower Shop chatbot running in your Watson
Assistant service. That's great, but how do we place it on an actual site?

WordPress is a content management system that allows anyone to quickly have a website up
and running. This platform has a lot of features out of the box, and many more can be added
through plugins.

We developed one such a plugin for Watson Assistant to make it extremely easy to place a
chatbot on a WordPress site.

We'll discuss the plugin in the next lab. But first, we need to create a WordPress site.

In the next section of this module, you’ll find a button that will allow you to **_Generate a
WordPress site_**. Click on it to generate your site.
```
Do not create a WordPress.com site. Generate the site using the tool provided. WordPress.com
expects you to pay to be able to install plugins. The WordPress(.org) installation we give you
already has the plugin installed.
```

You'll be given details about your site, similarly to the ones shown in the figure below.

<img src="images/8.9.PNG" width="600">

Please **make note of these WordPress credentials** you'll be given upon generating the site,
you'll need them to log into the site in the next lab.

In particular, write down somewhere your generated WordPress _Dashboard URL_ (where you'll
log in), your _username_ , and your _password_. (Note that these are WordPress credentials and
therefore different from the API ones you wrote down earlier in this lab).

If you lose them, you can always come back to the next section of this module (i.e., **_Generate a
WordPress site_** ) and obtain them again.


Without further ado, go ahead to the next section and actually generate your WordPress site.

Please note that this site is for testing purposes only. Do not use it as your main site as it might
be shut down after a certain period of time.

# A note about Assistants and Skills

Assistants have one or more skills. Skills are linked to particular assistants.

You don’t normally have to worry about this because a default assistant and a skill (already
linked to each other) were automatically generated for us when we created our Watson
Assistant instance.

If, in the future, you were to create a new assistant, you’ll want to make sure to link it to a skill.

Note that when you deploy your chatbot, you generally want to use the credentials from the
assistant (like we did in this lab) and not from its skill.


