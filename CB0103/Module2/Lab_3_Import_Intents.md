<img src= "images/IDSNlogo.PNG" width = 300>

#### Lab 3: Import Intents

Exercise 1: Add intents from the Content Catalog

IBM provides you with some ready-made intents that might be relevant to the scope of your
chatbot. To see what's available, **click on** **_Content Catalog_** within your dialog skill.

<img src="images/3.1.PNG" width="600"/>

Select one category of your choice (e.g., _Banking_ ) and then **click on the** **_Add to skill_** **button** next
to it. Switch back to the _Intents_ section (in top left of the page) and you should see a series of
new intents relevant to common queries customers may have for the category of your choice.

<img src="images/3.2.PNG" width="600"/>

This isn't quite a pre-made chatbot but it's a nice starting point, that you can edit and adapt as
needed. Feel free to try them out in the _Try it out_ panel.

For example, if you added banking intents, try I lost my credit card in the _Try it out_ panel.
What intent is detected by Watson?

As usual, ignore the response that we get from the chatbot. It’s simply because we haven’t
addressed responses yet. What we care about at this stage is that Watson correctly identifies
and classifies our input.

We are not going to use banking intents for our flower shop chatbot so select the checkmarks
next to them and **press the Delete button to remove them**. (Make sure you keep the chit chat
intents we created.)

<img src="images/3.3.PNG" width="600"/>


Confirm the deletion when prompted.

<img src="images/3.4.PNG" width="600"/>

You'll notice, in the screenshot on the previous page, how you were also given the option to
export the selected intents. This is quite useful when reusing intents across different chatbots.
Particularly when they are intents you created and not pre-made, as the ones in the Content
Catalog will also be available in your new chatbot so there is no need to export and import
them.

Go ahead and select our three chitchat intents and **click on the** **_Export_** **button** to download a
CSV file containing our intents and examples.

<img src="images/3.5.PNG" width="600"/>


Open the file to see what it looks like.

<img src="images/3.6.PNG" width="400"/>

You’ll notice that the structure is very simple. The example is in the first column, and the intent
it corresponds to, is in the second column. If you open the CSV file in a text editor, you’ll see its
raw form.

<img src="images/3.7.PNG" width="400"/>

As you can see, it's very easy to create, modify, and delete intents, whether they were manually
created or imported from the Content Catalog.

Exercise 2: Import intents from a CSV file

Just like we exported our intents to a CSV file, we can do the opposite and import intents from
a CSV file. This format is particularly handy because it allows you to easily import intents (and
their examples) from a spreadsheet. Let's see how this works in practice.

1. **Download the CSV file** I prepared for you or copy and paste the following in a
hours_and_location_intents.csv file.

When do you open,hours_info
When are you open,hours_info
What days are you closed on?,hours_info
When do you close,hours_info
Are you open on Christmas' Day?,hours_info
Are you open on Saturdays?,hours_info
What time are you open until?,hours_info
What are your hours of operation?,hours_info
What are your hours?,hours_info
Are you open on Sundays?,hours_info
what are you hours of operation in Toronto,hours_info
what are the hours of operation for your Montreal store,hours_info
list of your locations,location_info
Where are your stores?,location_info
Where are you physically located?,location_info
What are your locations?,location_info
List of location,location_info


Give me a list of locations,location_info
Locations in Canada,location_info
locations in America,location_info
what's the address of your Vancouver store?,location_info
What's the address of your Toronto store?,location_info
do you have a flower shop in Montreal,location_info
where is your Toronto store?,location_info

If clicking on the link simply opened the file in a new tab in your browser instead of
downloading it, with that tab selected, press CRTL+S on Windows or ⌘+S on Mac to download
it.

You'll notice that the structure of the file is very simple. Each line of the file has an example,
comma separated by the intent we want to assign to it. Just like the chitchat examples you’ve
seen in the previous page.

In our _Flower Shop Chatbot_ we want to allow people to inquire about hours of operation and
addresses of our flower shop stores, so this file includes examples for
both #hours_info and #location_info.

Note that the # prefix is not included in the CSV file. It will be automatically added by Watson to
the intent names when importing them.

2. From the _Intents_ section of your skill, **click on the** **_Import intents_** **icon** next to the _Add
intent_ button.

<img src="images/3.8.PNG" width="600"/>

3. **Select** **_Choose a file_** from the window that appears and select the CSV file you downloaded
on your local drive.


4. **Click on the** **_Import_** **button**. A report of what was imported will be shown as seen in the
picture below. Click on _Done_ to close the window. You now have successfully imported two new
intents and their examples to train Watson.

<img src="images/3.9.PNG" width="500"/>

Take a moment to **review the intents that were imported** and the examples for each of them.

Next, take them for a spin in the _Try it out_ panel. **Ask questions like you naturally would** to
inquire about store hours or address information. Does it recognize the intents we imported
well enough? Train Watson further by adding your own examples directly from the _Try it_ panel
when it fails to interpret them correctly.

At this point, our chatbot understands basic chitchat and it detects when a question is about
hours of operation vs when it's about location.


