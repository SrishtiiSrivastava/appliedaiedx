<img src= "images/IDSNlogo.PNG" width = 300>

#### Lab 2: Create Intents

Now that we have an Assistant and its dialog skill, we are ready to start creating our chatbot.
We’ll start off with intents.

# Exercise 1 : Enter your Flower Shop Skill

In order to add intents, we’ll need to access our _Flower Shop Skill_. There are a couple of ways to
do that:

## Method 1

From the _Skills_ tab of your Watson Assistant instance click on the tile for your skill.
<img src="images/2.1.PNG" width="600"/>

## Method 2

From the _Assistants_ tab of your Watson Assistant instance, click on your chatbot tile.
<img src="images/2.2.PNG" width="600">

You’ll find yourself in the _Flower Shop Chatbot_ assistant. From here, click on its skill tile.

<img src="images/2.3.PNG" width="600">

Either method will get you inside of your skill (as shown in the image below), where you’ll be
able to create intents, entities, and much more.

<img src="images/2.4.PNG" width="600">

By the way, if you don’t know how to get back to the Watson Assistant inteface, you simply
have to visit your dashboard (available at cloud.ibm.com), click on _Services_ , and launch your
Watson Assistant instance again.

# Exercise 2: Create, train, and test intents

Now that we are inside of the _Intents_ section of our skill, we can add intents in a few ways.
Namely, we can add them manually, import them from a CSV file, or import them from a pre-
made content catalog which includes industry-specific intents.

In this exercise, we'll focus on the most common way. That is, manually adding intents.

1. From the _Intents_ section of your dialog skill, **click on the** **_Create intent_** **button**. Here you'll be
able to define an _Intent name_ and a _Description_. What happens if you try to call the
intent #greeting us with a space in the name?

<img src="images/2.5.PNG" width="600">


That’s right! We can’t use spaces in intent names. So, we either need to use a single word for
our intent or add underscore characters to separate multiple words when these are necessary
to express what our intent is all about. In our case, we can get away with just calling the intent
#greetings. Let’s do that.

2. **Define a #greetings intent**. You can leave the description blank, as the name is descriptive
enough, and then **click the** **_Create intent_** **button.**
3. You'll be prompted to create some user examples to train Watson on the concept of
greetings. **Enter hello then click** **_Add example_****.** Repeat the process for other greeting examples
such as:
    - Hi
    - Hey
    - good morning
    - good afternoon

and so on. Make sure you add one example at the time.

You don't need to go overboard, especially on such a simple intent, but you should
always **include at least 5 examples**. Ideally more, particularly for more complex intents.

If you make a typo in one or two of your examples, don't worry. Keep the typos, as your users
are likely to do the same mistakes, so this ends up training Watson on a more realistic input set.
When you are done, you can **click the back-arrow icon at the top** to go back to your list of
intents.

<img src="images/2.6.PNG" width="600">

4. Once you click on the back-arrow icon, you’ll be sent back to the _Intents_ section where a list
of your intents is shown. Right now, it’s just #greetings so let’s add more.

<img src="images/2.7.PNG" width="600">

By using the _Create intent_ button, repeat the process to **add
the #thank_you and #goodbyes intents** with at least 5 appropriate examples each.

For #thank_you, you might use examples such as:

- thank you
- thanks
- thx
- cheers
- appreciate it

For #goodbyes, you might want to use:

- good bye
- bye
- see you
- c ya
- talk to you soon

At this point, you'll have the most basic chit chat intents a chatbot needs to have. The more the
merrier, of course, but this will do for now.

5. To test our intents, **click on the** **_Try it_** **button** in the top right. A chat panel will appear where
you can try user input and see how Watson analyses it and how our chatbot responds. We
haven't provided responses yet (we'll do so in the module on _Dialog_ ) but we can still use it to
test the classification of our intents. If you see a _Watson is training_ message, please wait for
Watson to finish training on your intent examples.
6. Go ahead and **try some greetings, thank you, and goodbye messages** in the panel. Feel free
to try both examples you provided and expressions that you haven't provided as examples. For
instance, try hola and aloha. Though specific to certain languages, they are common enough
internationally to be recognized as greetings by Watson.

Now, **try it with kia ora** , a common greeting in New Zealand, but not a globally adopted one.
Chances are Watson will categorize it as _Irrelevant_. If Watson miscategorized one example, as it
arguably did in this example, feel free to click on the V symbol next to the detected intent
to **assign a different intent to it**. This will add your utterance (e.g., kia ora) as an example for
the intent you picked (e.g., #greetings), further training Watson.

The image below shows the message we receive when we make an intent correction directly
from the _Try it out_ panel. The word _model_ is used to refer to the fact that Watson will use our
correction to further train its Machine Learning model.

<img src="images/2.8.PNG" width="300">

You can click on that intent name (e.g., #greetings) in the _Intents_ section of your skill to verify
that the example was indeed added automatically for you. Once done training, if you test the
same utterance again, Watson will correctly recognize the right intent this time.

<img src="images/2.9.PNG" width="300">

In the _Try it out_ panel, **what happens if you try a nonsensical input?** Randomly smash on the
keyboard if you have to. Personally, I produced the beautiful, cat-walking-on-the-keyboard
string dljkasdlsa dasldj alskdkas ld. Create your own masterpiece.

Watson will always try its hardest to match the user input to an existing intent, even if it's not a
perfect match. But if its confidence level in the best matching intent is very low (below 20%), it
will treat the input as _Irrelevant_ , as it is likely not relevant to any of our intents.

In the module on the _Dialog_ (i.e., Module 4), we'll find out how to deal with situations in which
the user enters a question that is irrelevant or outside the scope of our chatbot.

To conclude this exercise, click on an existing intent of your choice in _Intents_ , and **add one more
example** to it. Then, select the checkmark next to it, and you'll be given the option to delete it
(or even move it to a different intent).

Go ahead and **delete that example** as shown in the figure below.

<img src="images/2.10.PNG" width="600">


In the future, for more complex intents, you'll be able to add examples that originate from your
real customers' conversations with your virtual assistant (or your human customer care team)
to better train Watson on your business-specific intents.


