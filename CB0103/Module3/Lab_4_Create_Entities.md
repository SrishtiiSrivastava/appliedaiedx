<img src= "images/IDSNlogo.PNG" width = 300>

#### Lab 4 - Create_Entities

# Exercise 1: Create, modify, and delete entities

Entities recognize and capture specific pieces of information in the user input. In our flower
shop chain chatbot, people asking us about store hours and locations might provide a specific
location.

In our fictitious Flower Shop chain, we have stores in Toronto, Montreal, Calgary, and
Vancouver. So, when a user asks, Where is your Toronto store? we shouldn't ignore that
extra bit of information so that we can take the location into account when formulating a
response.

We can start by creating a @location entity for those cities.

1. **In your skill, click on** **_Entities_** to enter the entities section.
2. Here, **click the** **_Create entity_** **button**. Choose @location as the entity name (note that the @
symbol is automatically added for you). Leave _Fuzzy Matching_ enabled so that we can still
detect the city name even if the user misspells it. Finally, **click the** **_Create entity_** **button**.
3. You'll be prompted to enter entity values and possible synonyms. **Enter Montreal and then
click** **_Add Value_** to add this entity value to our entity.

<img src="images/4.1.PNG" width="600">

Generally speaking, you won't need a synonym for cities, but you might include some if the city
has common nicknames or if people refer to your store location by its street or neighbourhood
in the city. Nearby small cities and towns can also act as synonyms. After all, if people are


asking about your store in a nearby town, they might be happy with an answer for the nearest
city.

Essentially, a synonym is not necessarily the dictionary definition of synonym. Though those are
good candidates for synonyms as well when it makes sense. For example, we could have an
entity called @relationship and the entity value @relationship:mother with mom as a
synonym for that value. When the user enters a question including the word mom, Watson will
detect @relationship:mother (the entity value for that synonym).

4. **Repeat the process** for Calgary and Vancouver. Next, add Toronto as well. But for
Toronto, add Warden Avenue as a synonym, as shown in the picture below.

<img src="images/4.2.PNG" width="600">


Click on the back-arrow in the top-left to go back to your skill.

Open the _Try it out_ panel and wait for Watson to finish training. What happens if you try, hours
of operation of your warden ave store in the _Try it out_ panel? Even though we haven't
entered Warden Avenue spelled exactly as defined in the synonyms, fuzzy matching helps our
chatbot detect the right entity value. It's worth noting that entity values can also have patterns,
accessible from the _Synonyms_ drop-down, as shown in the image below.

<img src="images/4.3.PNG" width="600">

A pattern is an advanced feature that allows you to detect an entity value based not on a
specific string (e.g., its synonym) but rather on a specific pattern like a properly formatted
phone number, email address, or website address. If you are a programmer, it's worth noting
that you specify your pattern as a Regular Expressions (e.g., ^\(?([0-9]{3})\)?[-. ]?([0-
9]{3})[-. ]?([0-9]{4})$ to detect that a North American phone number was provided). If

you are not a programmer, you can safely ignore this advanced feature.

5. At any time you can click on an entity value to edit its name or synonym. Entities values are
allowed to have spaces in them. When you first create an entity value, you're given the option
to click on the _Show recommendations_ button to select some synonyms from a list provided by
Watson.

**Try out this feature**. Click on _Entities_ , click on the @location entity, then select the
@location:Vancouver entity value by clicking on it. A Watson icon will appear. Click on it as
shown in the picture below.

<img src="images/4.4.PNG" width="600">


Watson will make a few suggestions. For example, for Vancouver, it will suggest a few nearby
cities as well as other major cities in Canada. **Select Burnaby as a synonym** and then **click the**
**_Add selected_** **button.** Finally, click on the X icon to close the recommendation section.

6. Use the _Try it out_ panel to **test out these entity values**. Try entering, What are your hours
of operation in Montreal and Where is your Burnaby store located? to see how
Watson classifies that user question in terms of intents and entities.

7. **Practice creating a new entity of your choice with some values, and then deleting it.** The
process is very similar to that of intents.

Inside of _Entities,_ you would select the checkmark next to the entity you created and no longer
want, and then click the _Delete_ button that appears.

```
Don't delete @location as we’ll need it in our chatbot.
```
# Exercise 2: Take a look at system entities

System entities allow us to easily detect common specific pieces of information like dates,
times, numbers, currencies, etc. They are quite convenient when collecting information from
the user.

For example, a restaurant reservation booking chatbot might use @sys-date, @sys-time, and
@sys-number to detect the date, time, and party size for the reservation.

In a previous version of this course, we employed the @sys-location and @sys-person
entities, but they have since been deprecated.

Generally speaking, it's worth using a system entity if one fits the bill for what you are trying to
do. But if it makes your life more difficult due to your specific requirements, you're better off
creating your own custom entity as we did in Exercise 1.


