<img src= "images/IDSNlogo.PNG" width = 300>

#### Lab 5 : Import and Export Entities

# Exercise 1: Import and export entities via CSV files

Importing and exporting entities via CSV files works very similarly to intents. When you select
one or more entities by checking off the checkmarks next to them, you'll be offered the ability
to export them to CSV. Likewise, you can import them by clicking the _Import entities_ button
next to _Create entity_.

[**Click on this link**](https://gist.githubusercontent.com/acangiano/f5dc5861cb6b4934ba69ff8f8debf914/raw/5a63b11f558c208dc33c17193302aec3a5fde98e/occasion_and_relationship_entities.csv
) to download a CSV file with two new entities I prepared (i.e.,
@occasion and @relationship).

If clicking on the link opens the file in a new tab, ensure you save the file on your disk
(press CRTL+S on Windows or ⌘+S on Mac) and then **proceed with importing the new
entities** by clicking on the _Import entities_ button in the _Entities_ section.

<img src="images/5.1.PNG" width="600">


As usual, you’ll be asked to **select the CSV file you downloaded and then click** **_Import_****.**

Upon successful upload and import of your entities, you should see a confirmation dialog
similar to the one shown in the image below.

<img src="images/5.2.PNG" width="600">

Lab 5 : Import and Export Entities

Click _Done_ and you should see two new entities. We'll use these two entities later on in the
chatbot. For now, this will serve as a good exercise on importing entities.

The only real difference you need to be aware of, as far as importing goes, lies in the structure
of the CSV file. For intents, it was example,intent. Entities can optionally have synonyms and
patterns as well, so the structure is different.

If you open the CSV file I provided you with, you'll notice that each line has the entity name
first, followed by a value, followed by a comma separated list of synonyms if any have been
provided for that entity value. If a pattern is specified instead, it will also appear here.


